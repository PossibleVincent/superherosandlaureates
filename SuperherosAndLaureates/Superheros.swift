//
//  Superheros.swift
//  SuperherosAndLaureates
//
//  Created by Vincent Dong on 4/13/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import Foundation

struct Superheros : Codable {
    var name: String
    var age: Int
    var secretIdentity: String
    var powers: [String]
}
