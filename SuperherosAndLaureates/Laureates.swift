//
//  Laureates.swift
//  SuperherosAndLaureates
//
//  Created by Vincent Dong on 4/13/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import Foundation

struct Laureates : Codable {
    var id: Int
    var firstname: String
    var surname: String
    var born: Date
    var died: Date
    var bornCountry: String
    var bornCountryCode: String
    var bornCity: String
    var diedCountry: String
    var diedCountryCode: String
    var diedCity: String
    var gender: String
    var prizes:[String]
    
    struct prizes : Codable {
        var year: Int
        var category: String
        var share: Int
        var motivation: String
        var affiliations: [String]
        
        struct affiliations : Codable {
            var name: String
            var city: String
            var country: String
        }
    }
}
