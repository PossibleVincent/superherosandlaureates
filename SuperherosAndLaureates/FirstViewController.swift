//
//  FirstViewController.swift
//  SuperherosAndLaureates
//
//  Created by Vincent Dong on 4/13/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import UIKit

class FirstViewController: UITableViewController {
    
    let superherosInfo = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    var superheros: [Superheros] = []
    
    
    @IBAction func fetchSuperheros(_ sender: Any) {
        let urlSession = URLSession.shared
        let url = URL(string: superherosInfo)
        urlSession.dataTask(with: url!, completionHandler: displaySuperherosInTableView).resume()
    }
    
    func displaySuperherosInTableView(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        
        do {
            let decoder:JSONDecoder = JSONDecoder()
            superheros = try decoder.decode([Superheros].self, from: data!)
            print(superheros)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print(error)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return superheros.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "superherosCell", for: indexPath)
        
        let superhero = superheros[indexPath.row]
        cell.textLabel?.text = superhero.name
        cell.detailTextLabel?.text = "\(superhero.powers)"
        return cell
    }
    
}
