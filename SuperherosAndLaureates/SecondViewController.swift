//
//  SecondViewController.swift
//  SuperherosAndLaureates
//
//  Created by Vincent Dong on 4/13/19.
//  Copyright © 2019 VinceD. All rights reserved.
//

import UIKit

class SecondViewController: UITableViewController {

    let laureatesInfo = "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    var laureates: [Laureates] = []
    
    
    @IBAction func fetchLaureates(_ sender: Any) {
        let urlSession = URLSession.shared
        let url = URL(string: laureatesInfo)
        urlSession.dataTask(with: url!, completionHandler: displayLaureatesInTableView).resume()
    }
    
    func displayLaureatesInTableView(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        
        do {
            let decoder:JSONDecoder = JSONDecoder()
            laureates = try decoder.decode([Laureates].self, from: data!)
            print(laureates)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print(error)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laureates.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "laureatesCell", for: indexPath)
        
        let laureate = laureates[indexPath.row]
        cell.textLabel?.text = laureate.firstname + laureate.surname
        cell.detailTextLabel?.text = "\(laureate.born)" + "\(laureate.died)"
        return cell
    }
}
